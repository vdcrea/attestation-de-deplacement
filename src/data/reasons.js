export default [
  {
    title: 'Activité professionnelle',
    description: 'Déplacements entre le domicile et le lieu d’exercice de l’activité professionnelle, lorsqu’ils sont indispensables à l’exercice d’activités ne pouvant être organisées sous forme de télétravail (sur justificatif permanent) ou déplacements professionnels ne pouvant être différés'
  },
  {
    title: 'Faire mes courses',
    description: 'Déplacements pour effectuer des achats de première nécessité dans des établissements autorisés (liste sur gouvernement.fr)'
  },
  {
    title: 'Raison de santé',
    description: 'Déplacements pour motif de santé'
  },
  {
    title: 'Raison familiale',
    description: 'Déplacements pour motif familial impérieux, pour l’assistance aux personnes vulnérables ou la garde d’enfants'
  },
  {
    title: 'Activité physique ',
    description: 'Déplacements brefs, à proximité du domicile, liés à l’activité physique individuelle des personnes, à l’exclusion de toute pratique sportive collective, et aux besoins des animaux de compagnie'
  }
]
