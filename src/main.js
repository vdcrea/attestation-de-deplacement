import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'

import '@/plugins/f7'

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
