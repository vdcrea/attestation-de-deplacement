import Framework7 from 'framework7/framework7.esm.bundle'
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle'
import 'framework7/css/framework7.bundle.css'
import 'framework7-icons/css/framework7-icons.css'

Framework7.use(Framework7Vue)
